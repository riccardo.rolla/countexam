 
$conf = Read-Properties $PSScriptRoot\countexam.properties
$studies = import-csv  $PSScriptRoot\countexam.csv -delimiter ";"
$conf.studies=$studies
$conf.study=   [System.Collections.Generic.Dictionary[string,int]]::new();
foreach ($study in $studies) {
	$conf.study[$study.StudyInstanceUID] = 0;
}

Write-Host $conf
			

start-dicomserver -Port $conf.port -AET $conf.aet  -Environment $conf  -onCStoreRequest {
	param($request,$file,$association,$env)
	$attribute = read-dicom -DicomFile $file
	Write-Host 
	$env.study[$attribute.StudyInstanceUID]=$env.study[$attribute.StudyInstanceUID]+1
	 
	 [Dicom.Network.DicomStatus]::success
}



foreach ($study in $studies) {
  Write-Host $study.StudyInstanceUID
  move-dicom -Study -SopClassProvider $conf.pacs  -AET $conf.aet -moveTo  $conf.aet -StudyInstanceUID  $study.StudyInstanceUID
 }
foreach ($study in $studies) {
	$study  | Add-Member -NotePropertyName imageCount -NotePropertyValue  $conf.study[$study.StudyInstanceUID]
}
$studies | Export-Csv -Path .\out.csv -Delimiter ';' -NoTypeInformation